build:
	lessc -x styles.less > styles.css
	python compile.py

dist: build
	uglifyjs wx-share.dev.js -m --compress > wx-share.min.js
