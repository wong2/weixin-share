!function(root) {

  function insertCSS() {
    var css = '{CSS}';

    var elem = document.createElement('style');
    elem.setAttribute('type', 'text/css');

    if ('textContent' in elem) {
      elem.textContent = css;
    } else {
      elem.styleSheet.cssText = css;
    }

    var head = document.getElementsByTagName('head')[0];
    head.appendChild(elem);
  }

  function insertHTML() {
    var s = '<div class="weixin-share">' +
              '<div class="arrow"></div>' +
              '<div class="help-title"><span>点击右上角的「•••」按钮</span></div>' +
              '<a href="javascript:void(0)">' +
              '  <span class="img share">发送给朋友</span>' +
              '</a>' +
              '<a href="javascript:void(0)">' +
              '  <span class="img circle">分享到朋友圈</span>' +
              '</a>' +
              '<div class="close-btn" onclick="window.hideWechatTip()">x</div>' +
            '</div>';
    $('body').append(s);
  }

  function addOverlay() {
    $('body').append('<div class="wx-share-overlay"></div>')
  }

  function removeOverlay() {
    $('.wx-share-overlay').remove();
  }

  root.showWechatTip = function() {
    addOverlay();
    if ( $('.weixin-share').length ) {
      $('.weixin-share').show();
      return;
    }
    insertCSS();
    insertHTML();
  };

  root.hideWechatTip = function() {
    $('.weixin-share').hide();
    removeOverlay();
  };

}(window);
