!function(root) {

  function insertCSS() {
    var css = '.weixin-share{position:fixed;top:10px;left:10px;right:10px;padding:10px;border-radius:4px;background:#f4f4f4;text-align:center;z-index:1003}.weixin-share .arrow{position:absolute;right:12px;top:-11px;width:46px;height:50px;background-position:-5px -5px;background-repeat:no-repeat;background-image:url("http://doora.qiniudn.com/wx_sprite.png");background-size:88px 64px}@media (-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx){.weixin-share .arrow{background-image:url("http://doora.qiniudn.com/wx_sprite@2x.png")}}.weixin-share .help-title{height:50px;padding-right:47px;text-align:right}.weixin-share .help-title span{position:relative;top:13px;right:10px;color:#e58c7c;font-size:18px;font-weight:bold}.weixin-share a{width:calc(46%);color:#6b6f73;display:inline-block;text-decoration:none;font-size:13px}.weixin-share a img{position:relative;top:7px;left:-3px;width:22px}.weixin-share a span{padding:4px 10px 15px 28px;display:inline-block;text-align:left;color:#e58c7c}.weixin-share a span.img{background-repeat:no-repeat;background-image:url("http://doora.qiniudn.com/wx_sprite.png");background-size:88px 64px}@media (-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx){.weixin-share a span.img{background-image:url("http://doora.qiniudn.com/wx_sprite@2x.png")}}.weixin-share a span.img.share{background-position:-61px -37px}.weixin-share a span.img.circle{background-position:-61px -5px}.weixin-share .close-btn{position:absolute;background-color:#f4f4f4;width:25px;height:25px;margin-top:10px;line-height:23px;border-bottom-left-radius:3px;border-bottom-right-radius:3px;color:#e58c7c;cursor:pointer}.wx-share-overlay{width:100%;height:100%;z-index:1000;position:fixed;top:0;left:0;background-color:#000;opacity:.6}';

    var elem = document.createElement('style');
    elem.setAttribute('type', 'text/css');

    if ('textContent' in elem) {
      elem.textContent = css;
    } else {
      elem.styleSheet.cssText = css;
    }

    var head = document.getElementsByTagName('head')[0];
    head.appendChild(elem);
  }

  function insertHTML() {
    var s = '<div class="weixin-share">' +
              '<div class="arrow"></div>' +
              '<div class="help-title"><span>点击右上角的「•••」按钮</span></div>' +
              '<a href="javascript:void(0)">' +
              '  <span class="img share">发送给朋友</span>' +
              '</a>' +
              '<a href="javascript:void(0)">' +
              '  <span class="img circle">分享到朋友圈</span>' +
              '</a>' +
              '<div class="close-btn" onclick="window.hideWechatTip()">x</div>' +
            '</div>';
    $('body').append(s);
  }

  function addOverlay() {
    $('body').append('<div class="wx-share-overlay"></div>')
  }

  function removeOverlay() {
    $('.wx-share-overlay').remove();
  }

  root.showWechatTip = function() {
    addOverlay();
    if ( $('.weixin-share').length ) {
      $('.weixin-share').show();
      return;
    }
    insertCSS();
    insertHTML();
  };

  root.hideWechatTip = function() {
    $('.weixin-share').hide();
    removeOverlay();
  };

}(window);
